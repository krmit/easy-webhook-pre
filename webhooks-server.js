#!/usr/bin/node
"use strict";

const {readFileSync} = require("fs-extra");
const { join, resolve } = require('path');
require("colors");
const { execSync } = require('child_process');
const { chdir, cwd } = require('process');
const { parse } = require('yaml').default;

const moment = require('moment');
const http = require('http');
const crypto = require('crypto');
const jsome = require("jsome");
const {terminal} = require('print-error');

console.log(("["+moment().toISOString()+"]").bold + " " + "Restarting server for webhook ")

const config = parse(readFileSync(resolve(__dirname, "config.yml"), { encoding: "utf-8" }));
const secret = parse(readFileSync(resolve(__dirname, "secret.yml"), { encoding: "utf-8" }));



http.createServer(function (req, res) {
    req.on('data', function(chunk) {
		
		const hook_name = req.url.split("/")[1];
		let webhook = config.hooks[hook_name];

        if (req.headers['x-gitlab-token'] === secret[hook_name]) {
			console.log(("["+moment().toISOString()+"]").bold +" "+ webhook.log.green);
			const current_cwd = cwd();
		    const script_dir = resolve(webhook.dir);
			chdir(script_dir);
			try {
			    for(let command of webhook.script) {
                    execSync(command);
		        }
		    }
		    catch(e) {
				console.log(("["+moment().toISOString()+"]").bold + " " + "Error in script".red);
				terminal(e);
				console.log(("Webbhook: "+hook_name.bold+" give error.").red);
		    }
		    chdir(current_cwd);
        }
        else {
			console.log(("["+moment().toISOString()+"]").bold + " " + "Webhook access denied".red);
		}
    });

    res.end();
}).listen(config.port);

console.log(("["+moment().toISOString()+"]").bold + " " + "Server running on: ".green.bold+String(config.port).bold)


